#include "stdafx.h"
#include <iostream>
using namespace std;

//���� �������� ������
int input(int ** a, int ** b, int ** c, int x, int y)
{
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			cout << "������� �������� " << i << ";" << j << " ������� a\n";
			cin >> a[i][j];
			cout << "������� �������� " << i << ";" << j << "  ������� b\n";
			cin >> b[i][j];
			c[i][j] = 0;
		}
	}
	return 0;
}
//���� �������� ��������
int input_vec(int * vec, int * vec1, int * vec0, int x)
{
	int i = 0;
	while (i < x)
	{
		cout << "������� �������� " << i << "  ������� vec\n";
		cin >> vec[i];
		cout << "������� �������� " << i << "  ������� vec1\n";
		cin >> vec1[i];
		vec0[i] = 0;
		i++;
	}
	return 0;
}

//����� ���������� ������
int output(int ** f, int x, int y)
{
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			cout << f[i][j] << "  ";
		}
	}
	cout << "\n";
	return 0;
}
//����� ���������� ��������
int output_vec(int * f, int x)
{
	int i = 0;
	while (i < x)
	{
		cout << f[i] << "  ";
		i++;
	}
	cout << "\n";
	return 0;
}

//��������� ������
int matr_umn(int ** a, int ** b, int ** c, int h, int w)
{
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
		{
			//c[i][j] = 0;
			for (int k = 0; k < h; k++)
				c[i][j] += a[i][k] * b[k][j];
		}
	}
	return 0;
}

//�������� ������
int matr_sum(int ** a, int ** b, int ** c, int h, int w)
{
	for (int i = 0; i < h; i++)
	{
		for (int j = 0; j < w; j++)
		{
			c[i][j] = a[i][j] + b[i][j];
		}
	}
	getchar();
	return 0;
}

//��������� ��������
int vec_umn(int * vec, int * vec1, int * vec0, int x)
{
	int i = 0;
	while (i < x)
	{
		vec0[0] += vec[i] * vec1[i];
		i++;
	}
	return 0;
}

//�������� ��������
int vec_sum(int * vec, int * vec1, int * vec0, int x)
{
	for (int i = 0; i < x; i++)
	{
		vec0[i] = vec[i] + vec1[i];
	}
	return 0;
}

//��������� ������������ ��������
int vectorn_sum(int vec[3], int vec1[3], int c[3][3])
{
	for (int i = 0; i < 3; i++)
	{
		c[0][0] += vec[i] * vec1[i];

		c[0][0] = vec[1] * vec1[2] - vec1[1] * vec[2];
		c[1][0] = vec[0] * vec1[2] - vec1[0] * vec[2];
		c[2][0] = vec[0] * vec1[1] - vec1[0] * vec[1];
	}
	return 0;
}

//������� �� ������
int vec_mat(int ** a, int * vec, int * vec0, int x, int y)
{
	for (int i = 0; i < x; i++)
	{
		for (int j = 0; j < y; j++)
		{
			vec0[i] += vec[j] * a[i][j];
		}
	}
	return 0;
}


int main()
{
	setlocale(0, "");//������ �������� �������
	int h, w, h_vec;

	cout << "������� ���-�� ����� �������" << endl;
	cin >> h;
	cout << "������� ���-�� �������� �������" << endl;
	cin >> w;
	cout << "������� ���-�� ����� �������" << endl;
	cin >> h_vec;

	//������ �������
	int **a = new int*[h];
	for (int i = 0; i < h; i++)
	{
		a[i] = new int[w];
	}

	//������ �������
	int **b = new int *[h];
	for (int i = 0; i < h; i++)
	{
		b[i] = new int[w];
	}

	//������� �������
	int **c = new int *[h];
	for (int i = 0; i < h; i++)
	{
		c[i] = new int[w];
	}

	if ((a != NULL)&(b != NULL)&(c != NULL))
	{
		cout << "Good\n";
	}
	else
	{
		cout << "Bad\n";
	}


	//������ ������
	int * vec = new int[h_vec];

	//������ ������
	int * vec1 = new int[h_vec];

	//������� ������
	int * vec0 = new int[h_vec];

	cout << "1.��������� ������\n2.��������� ��������\n3.�������� ��������\n4.�������� ������\n5.������ �� �������\n6.��������� ������������\n";
	int x;
	cin >> x;
	if (x == 1)
	{
		input(a, b, c, h, w);
		matr_umn(a, b, c, h, w);

		cout << "��������� ������� a:\n";
		output(a, h, w);
		cout << "��������� ������� b:\n";
		output(b, h, w);
		cout << "��������� ������������ c:\n";
		output(c, h, w);
	}
	else if (x == 2)
	{
		input_vec(vec, vec1, vec0, h_vec);
		vec_umn(vec, vec1, vec0, h_vec);

		cout << "�������� ������� vec:\n";
		output_vec(vec, h_vec);
		cout << "�������� ������� vec1:\n";
		output_vec(vec1, h_vec);
		cout << "�������� ������������ vec0:\n";
		cout << vec0[0] << "\n";
	}
	else if (x == 3)
	{
		input_vec(vec, vec1, vec0, h_vec);
		vec_sum(vec, vec1, vec0, h_vec);

		cout << "�������� ������� vec:\n";
		output_vec(vec, h_vec);
		cout << "�������� ������� vec1:\n";
		output_vec(vec1, h_vec);
		cout << "�������� ����� vec0:\n";
		output_vec(vec0, h_vec);
	}
	else if (x == 4)
	{
		input(a, b, c, h, w);
		matr_sum(a, b, c, h, w);

		cout << "��������� ������� a:\n";
		output(a, h, w);
		cout << "��������� ������� b:\n";
		output(b, h, w);
		cout << "��������� ����� c:\n";
		output(c, h, w);
	}
	else if ((x == 5)&(h_vec == w))
	{
		input_vec(vec, vec1, vec0, h_vec);
		input(a, b, c, h, w);
		vec_mat(a, vec, vec0, h, w);

		cout << "�������� ������� vec:\n";
		output_vec(vec, h_vec);
		cout << "�������� ������� a:\n";
		output(a, h, w);
		cout << "�������� ������������ vec0:\n";
		output_vec(vec0, h_vec);
	}
	else if (x == 6)
	{
		int c1[3][3] = {
			0,0,0,
			0,0,0,
			0,0,0 };

		int i = 0;
		while (i < 3)
		{
			cout << "������� �������� " << i << "  ������� vec\n";
			cin >> vec[i];
			cout << "������� �������� " << i << "  ������� vec1\n";
			cin >> vec1[i];
			i++;
		}

		vectorn_sum(vec, vec1, c1);

		cout << "������ a:\n" << vec[0] << " " << vec[1] << " " << vec[2] << " " << "\n\n";
		cout << "������ b:\n" << vec1[0] << " " << vec1[1] << " " << vec1[2] << " " << "\n\n";
		cout << "�����: \n" << c1[0][0] << "\n" << c1[1][0] << "\n" << c1[2][0] << endl;
	}
	else
	{
		cout << "Error\n";
		getchar();
	}

	cout << "\nPress Enter to Exit";
	getchar();
	getchar();
	return 0;
}

